# README #

1/3スケールのNEC PC-8001mk2SR風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1985年1月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-8001mkIISR)
- [ボクたちが愛した、想い出のパソコン・マイコンたち](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1099025.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2sr/raw/439e377030050421d0209d9b14b10e7257c48ff2/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2sr/raw/439e377030050421d0209d9b14b10e7257c48ff2/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8001mk2sr/raw/439e377030050421d0209d9b14b10e7257c48ff2/ExampleImage.png)
